"""
File consists of several functions for the calculation rules of FAIR Quality KPIs
"""

from functions.classes import *

def test_function():
    """Test function to check module functionality"""
    print("You called the test function.")

def kpi_mass(system: LegoAssembly)->float:
    """
    Calculates the total mass of the system

    Args:
        system (LegoAssembly): LegoAssembly object that represents the system

    Returns:
        total_mass (float): Sum of masses of all components in the system in g

    Raises:
        TypeError: If an argument with unsupported type is passed
            (i.e. anything other than LegoAssembly).
    """
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_mass()")

    total_mass = 0
    for c in system.get_component_list(-1):
        total_mass += c.properties["mass [g]"]
    return total_mass # alternative: sum(c.properties["mass [g]"] for c in system.get_component_list(-1))

# Add new functions for calculating metrics

def kpi_price(System: LegoAssembly)->float:
    """
    Calculates the total price of the system

    Args:
        system (LegoAssembly): LegoAssembly object that represents the system

    Returns:
        total_price (float): Sum of prices of all components in the system in €


    """
    total_price = 0
    for c in System.get_component_list(-1):
        total_price  += c.properties["price [Euro]"]
        
    return(total_price)


def kpi_delivery_time(System: LegoAssembly)->float:
    """
    Determines the greatest delivery time of the system

    Args:
        system (LegoAssembly): LegoAssembly object that represents the system

    Returns:
        total_delivery_time (float): Greatest delivery time of all components in the system in days


    """
    delivery_time = 0
    for c in System.get_component_list(-1):
        if delivery_time < c.properties["delivery time [days]"]:
            delivery_time = c.properties["delivery time [days]"]
            
    return(delivery_time)

def kpi_weight(System: LegoAssembly)->float:
    """
    Calculates the total weight of the system

    Args:
        system (LegoAssembly): LegoAssembly object that represents the system

    Returns:
        total_weight (float): Sum of masses of all components in the system in g

    """
    total_weight = 0
    for c in System.get_component_list(-1):
        total_weight  += c.properties["mass [g]"]
        
    return(total_weight)


if __name__ == "__main__":
    """
    Function to inform that functions in this module is
    intended for import not usage as script
    """
    print(
        "This script contains functions for calculating the FAIR Quality KPIs."
        "It is not to be executed independently."
    )
